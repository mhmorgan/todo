/*
  MIT License

  Copyright (c) 2018 Magnus Hirth

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

package main

import (
	. "github.com/mattn/go-getopt"
	"os"
	"log"
	"fmt"
	"bufio"
	"strings"
	"io/ioutil"
	"bytes"
	"encoding/gob"
	"time"
	"os/exec"
	"sort"
	"regexp"
)

const todosFileName = "todos.bin"
const logFileName = "log.txt"

// Configuration variables
var (
	caseinsensitive bool
)

func main() {
	// Setup environment
	home, ok := os.LookupEnv("HOME")
	if !ok {
		log.Fatalln("Couldn't fine a home directory!")
	}
	workPath := home + "/.todo"
	if err := os.Chdir(workPath); os.IsNotExist(err) {
		if err := os.Mkdir(workPath, os.ModeDir|0755); err != nil {
			log.Fatal(err)
		}
		log.Printf("created directory \"%s\"", workPath)
		if err := os.Chdir(workPath); err != nil {
			log.Fatal(err)
		}
	} else if err != nil {
		log.Fatal(err)
	}

	logFile, err := os.OpenFile(logFileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	log.SetOutput(logFile)
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Handle the arguments
	if len(os.Args) < 2 {
		PrintStatus()
	}
	opts := "cd:e:hiln:s"
	for c := Getopt(opts); c != EOF; c = Getopt(opts) {
		switch c {
		case 'b':
			RunInBackground()
		case 'c':
			CleanTodos()
		case 'e':
			EditTodo(OptArg)
		case 'd':
			DeleteTodo(OptArg)
		case 'h':
			PrintHelp()
		case 'i':
			caseinsensitive = true
		case 'l':
			PrintAllTodos()
		case 'n':
			NewTodo(OptArg)
		case 's':
			PrintStatus()
		default:
			PrintUsage()
		}
	}
}

/*******************************************************************************
 *
 * File handling
 *
 ******************************************************************************/

func GetTodos() []Todo {
	b, err := ioutil.ReadFile(todosFileName)
	if err != nil {
		log.Println(err)
		return []Todo{}
	}
	return BytesToTodos(b)
}

func WriteTodos(todos []Todo) {
	b := TodosToBytes(todos)
	if b != nil {
		err := ioutil.WriteFile(todosFileName, b, 0644)
		if err != nil {
			log.Println(err)
		}
	}
}

func TodosToBytes(todos []Todo) []byte {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	if err := enc.Encode(todos); err != nil {
		log.Println(err)
	}
	return buf.Bytes()
}

func BytesToTodos(b []byte) []Todo {
	var todos []Todo
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	if err := dec.Decode(&todos); err != nil {
		log.Println(err)
	}
	return todos
}

/*******************************************************************************
 *
 * Create new
 *
 ******************************************************************************/

// ISO 8601 Standard time format (YYYY-MM-DDThh:mm)
const isoTimeFormat = "2006-01-02T15:04"

var timeFormats = []string{
	"2006-01-02T15:04",
	"2006/01/02T15:04",
	"2006/01/02 15:04",
	"2006 01 02 15:04",
	"2006-01-02 15:04",
	"2006/01/02 15",
	"2006 01 02 15",
	"2006-01-02 15",
	"2006-01-02",
	"2006 01 02",
	"2006/01/02",
	"01-02",
	"01 02",
	"01/02",
	"01-02 15",
	"01 02 15",
	"01/02 15",
	"02 Jan",
	"Jan 02",
	"02 Jan 2006",
	"2006 Jan 02",
	"Jan 02 2006",
	"02 Jan 15",
	"Jan 02 15",
	"02 Jan 15:04",
	"Jan 02 15:04",
	"02 Jan 2006 15",
	"2006 Jan 02 15",
	"Jan 02 2006 15",
	"02 Jan 2006 15:04",
	"2006 Jan 02 15:04",
	"Jan 02 2006 15:04",
	"15:04",
}

// Reads a line from terminal and checks if it matches any valid time format
func StrToTime(str string) (t time.Time, ok bool) {
	var err error

	// Special non-time-format cases are checked with regex
	nowRe := regexp.MustCompile(`(?i)^\s*now\s*$`)
	tomorrowRe := regexp.MustCompile(`(?i)^\s*(tomorrow|(next )?day)\s*$`)
	yesterdayRe := regexp.MustCompile(`(?i)^\s*yesterday\s*$`)
	weekRe := regexp.MustCompile(`(?i)^\s*(next )?week\s*$`)
	monthRe := regexp.MustCompile(`(?i)(next )?month\s*$`)
	yearRe := regexp.MustCompile(`(?i)(next )?year\s*$`)

	if nowRe.MatchString(str) {
		return time.Now().Round(time.Minute), true
	}
	if tomorrowRe.MatchString(str) {
		return time.Now().AddDate(0, 0, 1).Round(time.Minute), true
	}
	if yesterdayRe.MatchString(str) {
		return time.Now().AddDate(0, 0, -1).Round(time.Minute), true
	}
	if weekRe.MatchString(str) {
		return time.Now().AddDate(0, 0, 7).Round(time.Minute), true
	}
	if monthRe.MatchString(str) {
		return time.Now().AddDate(0, 1, 0).Round(time.Minute), true
	}
	if yearRe.MatchString(str) {
		return time.Now().AddDate(1, 0, 0).Round(time.Minute), true
	}

	str = strings.TrimSpace(str)
	for _, f := range timeFormats {
		if t, err = time.Parse(f, str); err == nil {
			ok = true
			break
		}
	}
	year, mon, day := t.Date()
	if year == 0 && mon == 1 && day == 1 {
		day = time.Now().Day()
		mon = time.Now().Month()
	}
	if year == 0 {
		year = time.Now().Year()
	}
	t = time.Date(
		year,
		mon,
		day,
		t.Hour(),
		t.Minute(),
		0,
		0,
		time.Now().Location(),
	)
	return t, ok
}

// Creates a hash value from the given string.
func NameHash(s string) int {
	var hash int
	for i, b := range []byte(s) {
		hash += int(b) * (i + 1)
	}
	return hash
}

func GeneratePattern() []time.Duration {
	var ptn []time.Duration
	var prev time.Time

	fmt.Println("Pattern types (1) Hourly (2) Daily (3) Weekly (4) Monthly (5) Yearly (6) Every minute (0) Custom")
	in := getLine("Choose pattern")
	switch in {
	case "1":
		fmt.Println("Generating a reminder hourly.")
		ptn = []time.Duration{time.Hour}
	case "2":
		fmt.Println("Generating a reminder daily.")
		ptn = []time.Duration{24 * time.Hour}
	case "3":
		fmt.Println("Generating a reminder weekly.")
		ptn = []time.Duration{7 * 24 * time.Hour}
	case "4":
		fmt.Println("Generating a reminder monthly.")
		ptn = []time.Duration{30 * 24 * time.Hour}
	case "5":
		fmt.Println("Generating a reminder yearly.")
		ptn = []time.Duration{365 * 30 * 24 * time.Hour}
	case "6":
		fmt.Println("Generating a reminder every minute.")
		ptn = []time.Duration{time.Minute}
	default:
		fmt.Printf("Enter at least two dates to generate a pattern.\n" +
			"An empty input ends the generation.\n")
		for {
			if in := getLine("Enter date/time"); in == "" {
				break
			} else {
				t, ok := StrToTime(in)
				if !ok {
					fmt.Print("Invalid! ")
					continue
				}
				if !prev.IsZero() {
					ptn = append(ptn, t.Sub(prev))
				}
				prev = t
			}
		}
	}
	return ptn
}

func TimesFromPattern(ptn []time.Duration) []time.Time {
	var idx int
	var times []time.Time

	start, ok := StrToTime(getLine("Enter start date/time"))
	for !ok {
		start, ok = StrToTime(getLine("Invalid! Enter valid start date/time"))
	}
	end, ok := StrToTime(getLine("Enter end date/time"))
	for !ok {
		end, ok = StrToTime(getLine("Invalid! Enter valid end date/time"))
	}

	tmp := start
	for end.After(tmp) || end.Equal(tmp) {
		times = append(times, tmp)
		tmp = tmp.Add(ptn[idx])
		idx++
		if idx >= len(ptn) {
			idx = 0
		}
	}
	return times
}

// Create a new thing to do. The user may add deadline, reminders and a description.
func NewTodo(title string) {
	var times []time.Time

	todos := GetTodos()
	for _, todo := range todos {
		if todo.Title == title {
			fmt.Printf("\"%s\" already exists.\n", title)
			return
		}
	}

	todo := Todo{Title: strings.TrimSpace(title)}
	log.Printf("New todo: \"%s\"\n", title)
	fmt.Printf("New todo: \"%s\"\n", title)
	fmt.Println("(y - yes, p - pattern, n - no)")

	// Use regular expression to evaluate user input in a more dynamic manner
	noRe := regexp.MustCompile(`(?i)^\s*no?\s*$`)
	patternRe := regexp.MustCompile(`(?i)^\s*p(attern)?\s*$`)

	// if getLine("Add deadline? (Y/n)") != "n" {
	if !noRe.MatchString(getLine("Add deadline? (Y/n)")) {
		t, ok := StrToTime(getLine("Enter deadline"))
		for !ok {
			t, ok = StrToTime(getLine("Invalid! Enter valid deadline"))
		}
		todo.Deadline = t
		todo.HasDeadline = true
	}

	// Add times of reminders
	for {
		in := getLine("Add reminder? (Y/p/n)")
		if noRe.MatchString(in) {
			break
		} else if patternRe.MatchString(in) {
			ptn := GeneratePattern()
			ts := TimesFromPattern(ptn)
			times = append(times, ts...)
		} else {
			t, ok := StrToTime(getLine("Enter reminder"))
			for !ok {
				t, ok = StrToTime(getLine("Invalid! Enter valid reminder"))
			}
			times = append(times, t)
		}
		todo.HasReminders = true
	}
	todo.Reminders = NewTimeQueue(times)

	var i int
	for {
		todo.File = fmt.Sprintf("%08x.txt", NameHash(title)+i)
		f, e := os.Open(todo.File)
		if os.IsNotExist(e) {
			break
		}
		f.Close()
		i++
	}
	if !noRe.MatchString(getLine("Open in text editor? (Y/n)")) {
		openInEditor(todo.File)
	}

	todos = append(todos, todo)
	sort.Slice(todos, func(i, j int) bool {
		return todos[j].Deadline.After(todos[i].Deadline)
	})
	WriteTodos(todos)
}

/*******************************************************************************
 *
 * Edit and delete
 *
 ******************************************************************************/

// Searches for a single match with the "title" string.
// Informs the user of any error.
func FindTodo(title string) (todo Todo, idx int, ok bool) {
	var matches []Todo
	var strMatch func(ls, rs string) bool

	if caseinsensitive {
		strMatch = func(ls, rs string) bool {
			ciLs := strings.ToLower(ls)
			ciRs := strings.ToLower(rs)
			return strings.Contains(ciLs, ciRs)
		}
	} else {
		strMatch = func(ls, rs string) bool {
			return strings.Contains(ls, rs)
		}
	}

	todos := GetTodos()
	for i, t := range todos {
		if strMatch(t.Title, title) {
			matches = append(matches, t)
			todo = t
			idx = i
		}
	}
	if len(matches) == 0 {
		fmt.Printf("No match for \"%s\"\n", title)
	} else if len(matches) > 1 {
		fmt.Printf("Multiple matches for \"%s\":\n", title)
		for _, todo := range matches {
			fmt.Printf("\t%s\n", todo.Title)
		}
	} else {
		ok = true
	}
	return todo, idx, ok
}

// Opens the file of the thing to do which matches 'title'
func EditTodo(title string) {
	if todo, _, ok := FindTodo(title); ok {
		openInEditor(todo.File)
		log.Printf("Edited \"%s\"\n", todo.Title)
	}
}

// Returns a new slice of things to do without element i
func RemoveTodo(idx int, todos []Todo) []Todo {
	var tmp []Todo

	if idx < 0 || idx >= len(todos) {
		return todos
	}

	if err := os.Remove(todos[idx].File); err != nil {
		log.Println(err)
	}
	tmp = todos[:idx]
	if idx < len(todos)-1 {
		tmp = append(tmp, todos[idx+1:]...)
	}
	return tmp
}

// Removes the thing to do which matches 'title'
func DeleteTodo(title string) {
	if todo, i, ok := FindTodo(title); ok {
		noRe, err := regexp.Compile(`(?i)^\s*no?\s*$`)
		if err != nil {
			log.Println(err)
			return
		}
		if noRe.MatchString(getLine("Delete \"%s\"? (Y/n)", todo.Title)) {
			return
		}

		todos := GetTodos()
		todos = RemoveTodo(i, todos)
		WriteTodos(todos)
		fmt.Printf("Deleted \"%s\"\n", todo.Title)
		log.Printf("Deleted \"%s\"\n", todo.Title)
	}
}

/*******************************************************************************
 *
 * Terminal output
 *
 ******************************************************************************/

// A list of todos with deadlines on ISO 8601 format
func IsoList(todos []Todo) string {
	var str string
	var maxLen int
	for _, t := range todos {
		if len(t.Title) > maxLen {
			maxLen = len(t.Title)
		}
	}
	for _, t := range todos {
		if t.HasDeadline {
			str += fmt.Sprintf("  %-*s (%s)\n", maxLen, t.Title, t.DlIso())
		} else {
			str += fmt.Sprintf("  %-*s\n", maxLen, t.Title)
		}
	}
	return str
}

// A list of all the todos which haven't got a deadline
func NondeadlineList(todos []Todo) string {
	var str string
	for _, t := range todos {
		if !t.HasDeadline {
			str += fmt.Sprintf("  %s\n", t.Title)
		}
	}
	return str
}

// A list of all the todos with human friendly strings for the deadlines
func StringList(todos []Todo) string {
	var str string
	var maxLen int
	for _, t := range todos {
		if len(t.Title) > maxLen {
			maxLen = len(t.Title)
		}
	}
	for _, t := range todos {
		if t.HasDeadline {
			str += fmt.Sprintf("  %-*s (%s)\n", maxLen, t.Title, t.DlString())
		} else {
			str += fmt.Sprintf("  %-*s\n", maxLen, t.Title)
		}
	}
	return str
}

func StringListNonaligned(todos []Todo) string {
	var str string
	for _, t := range todos {
		if t.HasDeadline {
			str += fmt.Sprintf("  %s (%s)\n", t.Title, t.DlString())
		} else {
			str += fmt.Sprintf("  %s\n", t.Title)
		}
	}
	return str
}

// Prints a list with all todos
func PrintAllTodos() {
	todos := GetTodos()
	fmt.Println("All todos:")
	fmt.Print(IsoList(todos))
	log.Println("Printed all todos")
}

const (
	day   = 24 * time.Hour
	week  = 7 * day
	month = 4 * week
)

// Prints a list with all relevant thing to do. Relevant means things without
// a deadline and things with deadline within one month ahead in time.
func PrintStatus() {
	var todos []Todo
	var maxLen int

	ts := GetTodos()
	for _, t := range ts {
		if t.DeadlineUpcoming(month) {
			todos = append(todos, t)
			if len(t.Title) > maxLen {
				maxLen = len(t.Title)
			}
		}
	}
	sort.Slice(todos, func(i, j int) bool {
		return todos[j].Deadline.After(todos[i].Deadline)
	})
	fmt.Println("Todo's nearing their deadline:")
	fmt.Print(StringList(todos))
	fmt.Println("Todo's without deadlines:")
	fmt.Print(NondeadlineList(ts))
	log.Println("Printed status update")
}

func PrintHelp() {
	fmt.Print("Usage:\n\n" +
		"\ttodo [OPTION]... [NAME]\n\n" +
		"Options:\n\n" +
		"\t-b         Start the program in backup mode, in which it will show reminders in popup windows.\n" +
		"\t-c         Clean up by removing out-of-date todos\n" +
		"\t-d NAME    Delete a todo which matches NAME. It doesn't delete if NAME matches more than one todo.\n" +
		"\t-e NAME    Edit the file of the todo which matches NAME.\n" +
		"\t-h         Print this message.\n" +
		"\t-i         Make name matching caseinsensitive.\n" +
		"\t-l         Print a list with all todos.\n" +
		"\t-n NAME    Creates a new thing to do with the given NAME. The user may add a deadline, reminders\n" +
		"\t             and edit the new todo's file. When typing a date/time must be in a valid format.\n" +
		"\t             02 Jan 2006 15:04 is valid. Valid variations are: The month may be written in a two-decimal number.\n" +
		"\t             hh:mm without date is allowed. Minutes may be omitted in a date. The year may be ommited when\n" +
		"\t             writing a date, but not the month or day. Some words like now, tomorrow, next week etc. are valid.\n" +
		"\t-s         Print a status update. Prints todos without a deadline and the todos which are nearing\n" +
		"\t             their deadline.\n")
}

func PrintUsage() {
	PrintHelp()
}

/*******************************************************************************
 *
 * Clean outdated todos
 *
 ******************************************************************************/

func CleanTodos() {
	var removes []Todo
	var cnt int
	todos := GetTodos()
	now := time.Now()
	l := len(todos)
	fmt.Println("Removing todo's with expired deadlines...")
	for i := 0; i < l; i++ {
		if todos[i].HasDeadline && now.After(todos[i].Deadline) {
			removes = append(removes, todos[i])
			todos = RemoveTodo(i, todos)
			cnt++
			l--
			i--
		}
	}
	fmt.Print(IsoList(removes))
	log.Printf("Cleaning %d todo(s)\n", cnt)
	WriteTodos(todos)
}

/*******************************************************************************
 *
 * Run in background
 *
 ******************************************************************************/

const bckgTick = time.Minute

// Used to run this program in the background this functions runs in an infinite
// loop which iterates at an rate specified by bckgTick. It detects and informs
// the user of reminders.
func RunInBackground() {
	for range time.Tick(bckgTick) {
		var cnt int
		var todos []Todo
		ts := GetTodos()
		for i, t := range ts {
			if t.PendingReminder() {
				todos = append(todos, t)
				ts[i].UpdateReminders()
				cnt++
			}
		}
		if len(todos) > 0 {
			log.Printf("Reminding about %d todo(s)\n", cnt)
			showPopup(StringListNonaligned(todos))
			WriteTodos(ts)
		}
	}
}

/*******************************************************************************
 *
 * My little helpers
 *
 ******************************************************************************/

// Gets a line from the user in the terminal. The terminating newline is removed.
func getLine(prompt string, args ...interface{}) string {
	var str string
	str = fmt.Sprintf(prompt, args...)
	fmt.Printf("%s: ", str)

	rd := bufio.NewReader(os.Stdin)
	str, err := rd.ReadString('\n')
	if err != nil {
		log.Println(err)
	}
	return strings.TrimSuffix(str, "\n")
}

// Opens the given file in vim
func openInEditor(file string) {
	if file == "" {
		fmt.Println("Cannot edit file without name.")
		return
	}
	editor := exec.Command("vim", file)
	editor.Stdin = os.Stdin
	editor.Stdout = os.Stdout
	if err := editor.Run(); err != nil {
		log.Println(err)
	}
}

// Shows a info popup window using zenity
func showPopup(msg string) {
	text := fmt.Sprintf("--text=%s", strings.TrimSpace(msg))
	title := "--title=Todo Reminder"
	popup := exec.Command("zenity", "--info", "--no-wrap", title, text)
	if err := popup.Run(); err != nil {
		log.Println(err)
	}
}

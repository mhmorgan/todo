package main

import (
	"time"
	"sort"
	"fmt"
)

type Todo struct {
	Title        string
	File         string
	HasDeadline  bool
	Deadline     time.Time
	HasReminders bool
	Reminders    TimeQueue
}

func (td Todo) DlIso() string {
	if !td.HasDeadline {
		return ""
	}
	return " " + td.Deadline.Format(isoTimeFormat) + " "
}

func (td Todo) DlString() string {
	var str string = " "
	var d, m, y int = 24, 24*30, 24*365

	if !td.HasDeadline {
		return ""
	}

	until := time.Until(td.Deadline).Round(time.Minute)
	min := int(until.Minutes()) % 60
	hour := int(until.Hours())
	year := hour / y
	hour %= y
	mon := hour / m
	hour %= m
	day := hour / d
	hour %= d

	if year == 1 {
		str += fmt.Sprintf("%d year ", year)
	} else if year > 1 {
		str += fmt.Sprintf("%d years ", year)
	}
	if mon == 1 {
		str += fmt.Sprintf("%d month ", mon)
	} else if mon > 1 {
		str += fmt.Sprintf("%d months ", mon)
	}
	if day == 1 {
		str += fmt.Sprintf("%d day ", day)
	} else if day > 1 {
		str += fmt.Sprintf("%d days ", day)
	}
	if hour == 1 {
		str += fmt.Sprintf("%d hour ", hour)
	} else if hour > 1 {
		str += fmt.Sprintf("%d hours ", hour)
	}
	if min == 1 {
		str += fmt.Sprintf("%d minute ", min)
	} else if min > 1 {
		str += fmt.Sprintf("%d minutes ", min)
	} else if str == " " {
		str = " now "
	}

	return str
}

func (td Todo) DeadlineUpcoming(d time.Duration) bool {
	if !td.HasDeadline {
		return false
	}
	now := time.Now()
	return now.Add(d).After(td.Deadline) && td.Deadline.After(now)
}

func (td Todo) CurrentReminder() time.Time {
	if td.HasReminders {
		return td.Reminders.Current()
	}
	return time.Time{}
}

func (td Todo) PendingReminder() bool {
	if td.HasReminders {
		return td.Reminders.Pending()
	}
	return false
}

func (td *Todo) UpdateReminders() {
	if td.HasReminders {
		td.Reminders.Update()
	}
}

// TimeQueue keeps track of an index and an array of times
type TimeQueue struct {
	Index int
	Times []time.Time
}

func (tq TimeQueue) Current() time.Time {
	if tq.Index < len(tq.Times) {
		return tq.Times[tq.Index]
	}
	return time.Time{}
}

func (tq TimeQueue) Pending() bool {
	if tq.Index < len(tq.Times) {
		return time.Now().After(tq.Times[tq.Index])
	}
	return false
}

func (tq *TimeQueue) Update() {
	if tq.Index < len(tq.Times) {
		for time.Now().After(tq.Times[tq.Index]) {
			tq.Index++
			if tq.Index >= len(tq.Times) {
				break
			}
		}
	}
}

func NewTimeQueue(times []time.Time) TimeQueue {
	queue := TimeQueue{Times: times}
	sort.Slice(queue.Times, func(i, j int) bool {
		return queue.Times[j].After(queue.Times[i])
	})
	return queue
}

# TODO

TODO is a todo list for linux.

```
Usage:

    todo [OPTION]... [NAME]

Options:

    -b         Start the program in backup mode, in which it will show reminders in popup windows.
    -c         Clean up by removing out-of-date todos
    -d NAME    Delete a todo which matches NAME. It doesn't delete if NAME matches more than one todo.
    -e NAME    Edit the file of the todo which matches NAME.
    -h         Print this message.
    -i         Make name matching caseinsensitive.
    -l         Print a list with all todos.
    -n NAME    Creates a new thing to do with the given NAME. The user may add a deadline, reminders
                and edit the new todo's file. When typing a date/time must be in a valid format.
                02 Jan 2006 15:04 is valid. Valid variations are: The month may be written in a two-decimal number.
                hh:mm without date is allowed. Minutes may be omitted in a date. The year may be ommited when
                writing a date, but not the month or day. Some words like now, tomorrow, next week etc. are valid.
    -s         Print a status update. Prints todos without a deadline and the todos which are nearing
                their deadline.
```
